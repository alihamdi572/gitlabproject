<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Location;
use App\Entity\Voiture;
use Symfony\Component\Panther\PantherTestCase;

class FunctionalTest extends PantherTestCase
{
    private $entityManager;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->entityManager = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testCreateClient()
    {
        $client = new Client();
        $client->setNom('John');
        $client->setPrenom('Doe');
        $client->setAdresse('123 Main Street');
        $client->setCin('ABC123');

        $this->entityManager->persist($client);
        $this->entityManager->flush();

        $this->assertNotNull($client->getId());
        $this->assertEquals('John', $client->getNom());
        // ... other assertions for Client
    }

    public function testAddLocation()
    {
        // Create a Client
        $client = new Client();
        $client->setNom('Jane');
        $client->setPrenom('Doe');
        $client->setAdresse('456 Main Street');
        $client->setCin('XYZ789');
        $this->entityManager->persist($client);

        // Create a Voiture
        $voiture = new Voiture();
        $voiture->setSerie('123ABC');
        $voiture->setDateMM(new \DateTime('2020-01-01'));
        $voiture->setPrixJour(100.0);
        $this->entityManager->persist($voiture);

        // Create a Location
        $location = new Location();
        $location->setDateD(new \DateTime('today'));
        $location->setDateA(new \DateTime('tomorrow'));
        $location->setPrix(200.0);
        $location->setClient($client);
        $location->setVoiture($voiture);

        $this->entityManager->persist($location);
        $this->entityManager->flush();

        $this->assertNotNull($location->getId());
        $this->assertSame($client, $location->getClient());
        $this->assertSame($voiture, $location->getVoiture());
        // ... other assertions for Location
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
